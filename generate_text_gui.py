import PySimpleGUI as sg
from helpers import disable_warnings
from models import generator_factory


def main():
    # sg.theme('DarkAmber')
    sg.theme('LightBlue3')
    # sg.theme('LightGray1')
    # sg.theme('LightGray')

    disable_warnings()
    methods = ['GPT2', 'markov', 'RNN']

    layout = [[sg.Col([[sg.T('Start of the text')], [sg.In('бувало', key='-IN-')]]),
               sg.Col([[sg.T('Chars')],
                       [sg.Input('400', key='-NUM-', enable_events=True, size=(5, 1))]])],
              [sg.Combo(values=methods, key='-MODEL-',
                        default_value=methods[2], readonly=True)],
              [sg.Button('Generate', bind_return_key=True), sg.Exit()]]

    window = sg.Window('Ukrainian text generation', layout,
                       return_keyboard_events=True)
    # window.bind('q', lambda event: window.destroy())

    # symbols_from_file = '!"\'(),-.:;?–—0123456789аАбБвВгГґҐдДеЕєЄжЖзЗиИіІїЇйЙкКлЛмМнНоОпПрРсСтТуУфФхХцЦчЧшШщЩъыЫьЬэЭюЮяЯ'

    while 42:
        event, values = window.read()  # type: ignore
        if event in (sg.WINDOW_CLOSED, 'Exit', 'Escape:9'):
            break
        if (event == '-NUM-' and values['-NUM-']
                and values['-NUM-'][-1] not in ('0123456789')):
            window['-NUM-'].update(values['-NUM-'][:-1])
        if event == 'Generate':
            text = generator_factory(values['-MODEL-']
                                     ).generate(values['-IN-'], int(values['-NUM-']))
            required_height = max((len(text) // 80) + 1, text.count('\n') + 1)
            sg.Window('Generated text',
                      [[sg.Multiline(text, size=(80, required_height),
                                     disabled=True)], [sg.Exit()]]
                      ).read(close=True)
    window.close()


if __name__ == "__main__":
    main()
