import tensorflow as tf
import numpy as np
from tqdm.auto import trange

def get_vocab(path_to_file="data/Shevchenko/Shevchenko_no_latin.txt"):
    text = open(path_to_file, "rb").read().decode(encoding="utf-8")
    return sorted(set(text))


def build_model(
    vocab_size, embedding_dim, rnn_layers, rnn_units, batch_size, stateful=True
):
    model = tf.keras.Sequential(
        [
            tf.keras.layers.Embedding(
                vocab_size, embedding_dim, batch_input_shape=[batch_size, None]
            )
        ]
        + [
            tf.keras.layers.GRU(
                rnn_units,
                return_sequences=True,
                stateful=stateful,
                recurrent_initializer="glorot_uniform",
            )
            for _ in range(rnn_layers)
        ]
        + [tf.keras.layers.TimeDistributed(tf.keras.layers.Dense(256))]
    )
    model.build()
    return model


def generate_text(model, start_string, vocab, num_generate=400):
    char2idx = {u: i for i, u in enumerate(vocab)}
    idx2char = np.array(vocab)

    input_eval = [char2idx[s] for s in start_string]
    input_eval = tf.expand_dims(input_eval, 0)

    text_generated = []

    # Increase to choose less probable characters more often
    temperature = 1.0

    model.reset_states()
    for _ in trange(num_generate, leave=False):
        # Get predictions for the model
        predictions = model(input_eval)
        # Remove unccecessary dimension (because batch_size == 1)
        predictions = tf.squeeze(predictions, 0)

        # Change the probabilities
        predictions = predictions / temperature
        # Choose the result
        predicted_id = tf.random.categorical(
            predictions, num_samples=1)[-1, 0].numpy()

        # Update input
        input_eval = tf.expand_dims([predicted_id], 0)

        text_generated.append(idx2char[predicted_id])

    return start_string + ''.join(text_generated)


embedding_dim = 2
rnn_layers = 3
rnn_units = 1096
