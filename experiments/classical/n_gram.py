import nltk

with open("../../data/Shevchenko/Shevchenko_no_latin.txt", "r") as f:
    text = f.read()

text = text.translate(dict.fromkeys(map(ord, '\n!"\'(),-.–—:;?'), ' '))
nltk.Text(text.split()).generate()
