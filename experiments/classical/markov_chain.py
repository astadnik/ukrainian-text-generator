from collections import defaultdict
import random

def markov_chain(text):
    words = text.split()
    collocation_dict = defaultdict(list)
    for current_word, next_word in zip(words[0:-1], words[1:]):
        collocation_dict[current_word].append(next_word)
    collocation_dict = dict(collocation_dict)
    return collocation_dict

def generate_sentence(chain, word_count, cur_word=None):
    cur_word = cur_word or random.choice(list(chain.keys()))
    sentence = cur_word.capitalize()
    for _ in range(word_count-1):
        next_word = random.choice(chain[cur_word])
        sentence += ' ' + next_word
        cur_word = next_word

    sentence += '.'
    return sentence

def main():
    with open("../../data/Shevchenko/Shevchenko_no_latin.txt", "r") as f:
        text = f.read()
    text = text.translate(dict.fromkeys(map(ord, '\n!"\'(),-.–—:;?'), ' '))
    print(generate_sentence(markov_chain(text), 100))

if __name__ == "__main__":
    main()
