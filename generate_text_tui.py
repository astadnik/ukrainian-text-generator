from helpers import parse_args, disable_warnings
from models import generator_factory, Generator

class TUI:
    def __init__(self, generator: Generator):
        self.generator : Generator = generator

    def run(self):
        text = self.generator.generate()
        print(text)

def main():
    args = parse_args()
    disable_warnings()
    # args.model = 'RNN'
    # args.input = 'бувало'
    # args.length = 100
    generator = generator_factory(args.model, args.input, args.length)
    TUI(generator).run()


if __name__ == "__main__":
    main()
