import plotly.express as px
import pandas as pd
import plotly.io as pio

########################
#  Number of articles  #
########################

# queries = ['NLP', 'English NLP', 'Ukrainian NLP']
# values = [3375, 365, 2]
# labels = {'x': 'Ключове слово', 'y': 'Кількість результатів'}
template = 'ggplot2'

# px.bar(
#     x=queries, y=values, text=values, labels=labels,
#     template=template,
# ).write_image(f'/home/astadnik/Pictures/n_articles.png', width=640,
#               height=480, scale=4)

######################
#  Markov vs n_gram  #
######################

# # df = pd.read_csv('experiments/classical/markov_vs_n_gram.csv')
# df = pd.read_json('experiments/classical/markov_vs_n_gram.json')
# # print(df.shape)
# # print(df.T)
# df = df.explode('times', True)
# # print(df.shape)
# # print(df)
# df['command'] = df['command'].apply(lambda s: s.replace('python ',
#     '').replace('.py', ''))
# df.rename({'command': 'model', 'times': 'time (s)'}, axis='columns', inplace=True)

# fig = px.box(df, y = 'time (s)', x='model', template=template)
# fig.write_image(f'/home/astadnik/Pictures/markov_vs_n_gram.png', width=640,
#               height=480, scale=4)

###############
#  RNN graph  #
###############

import tensorflow as tf
from experiments.models.RNN import embedding_dim, rnn_layers, rnn_units, build_model, get_vocab
vocab = get_vocab()
model = build_model(len(vocab), embedding_dim,
                    rnn_layers, rnn_units, batch_size=1)
tf.keras.utils.plot_model(model, '/home/astadnik/Pictures/model.png',
        show_shapes=True, show_dtype=True,
    show_layer_names=False, rankdir='TB', expand_nested=True, dpi=96
)
