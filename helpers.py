import argparse
def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-l", "--length", default=100, type=int,
                        help="Length of the text")
    parser.add_argument("-i", "--input", default='Бувало',
                        help="Input text to start from")
    parser.add_argument("-m", "--model", default='GPT2',
                        choices=['markov', 'RNN', 'GPT2'], help="Model to generate text")
    return parser.parse_args()


def disable_warnings():
    import os
    import logging
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
    import tensorflow as tf
    logging.disable(logging.CRITICAL)
    tf.get_logger().setLevel('INFO')
    tf.autograph.set_verbosity(1)
