from abc import ABC, abstractmethod

class Generator(ABC):
    def __init__(self, inp:str, lengh:int):
        self.inp = inp
        self.length = lengh

    @abstractmethod
    def generate(self):
        raise NotImplementedError(
            "You're using base class, please use a sublcass")


class Markov(Generator):
    def generate(self):
        from experiments.classical.markov_chain import generate_sentence, markov_chain
        with open("data/Shevchenko/Shevchenko_no_latin.txt", "r") as f:
            text = f.read()
        # text = text.translate(dict.fromkeys(map(ord, '\n!"\'(),-.–—:;?'), ' '))
        try:
            return generate_sentence(markov_chain(text), self.length, self.inp.lower())
        except KeyError:
            print('You should provide a word which is present in the dataset')
            return ''


class RNN(Generator):
    def generate(self):
        import tensorflow as tf
        from experiments.models.RNN import embedding_dim, rnn_layers, rnn_units, build_model, generate_text, get_vocab
        vocab = get_vocab()
        model = build_model(len(vocab), embedding_dim,
                            rnn_layers, rnn_units, batch_size=1)
        model.load_weights(tf.train.latest_checkpoint(
            'best_model_checkpoints'))
        model.build(tf.TensorShape([1, None]))
        return generate_text(model, self.inp, vocab, self.length)


class GPT2(Generator):
    def generate(self):
        import torch
        from transformers import (
            PreTrainedTokenizer, PreTrainedModel, AlbertTokenizer, GPT2LMHeadModel)
        tokenizer = AlbertTokenizer.from_pretrained("experiments/GPT2/fine_tuned")
        model = GPT2LMHeadModel.from_pretrained(
            "experiments/GPT2/fine_tuned",
            tokenizer_class="experiments/GPT2/fine_tuned",
        )
        assert(isinstance(tokenizer, PreTrainedTokenizer))
        assert(isinstance(model, PreTrainedModel))
        device = "cuda:0" if torch.cuda.is_available() else "cpu"
        inp_ids = tokenizer.encode(
            self.inp,
            add_special_tokens=False,
            return_tensors="pt",
        ).to(device)
        model.to(device)
        outputs = model.generate(
            inp_ids, do_sample=True, num_return_sequences=1, max_length=self.length
        )
        return tokenizer.decode(
            outputs[0], skip_special_tokens=True)


def generator_factory(name: str, inp: str, length: int) -> Generator:
    return {'GPT2': GPT2(inp, length), 'markov': Markov(inp, length), 'RNN': RNN(inp, length)}[name]
